package com.astroTransport.api.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.astroTransport.api.helloworld.HelloWorldController.BASE_PATH;

@RestController(BASE_PATH)
public class HelloWorldController {
    public static final String BASE_PATH = "/helloWorld";

    @GetMapping
    public String get() {
        return "HelloWorld";
    }

}
